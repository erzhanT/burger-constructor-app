import React, {useState} from 'react';
import {nanoid} from "nanoid";
import './App.css';
import {Container, Grid} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Ingredients from "../../components/Ingredients/Ingredients";


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    }
}));

const App = () => {
    const classes = useStyles();
    const [items, setItems] = useState([
        {id: nanoid(), name: 'Meat', price: 50, count: 0},
        {id: nanoid(), name: 'Cheese', price: 20, count: 0},
        {id: nanoid(), name: 'Salad', price: 5, count: 0},
        {id: nanoid(), name: 'Bacon', price: 30, count: 0}

    ]);

    const countIncrease = id => {
        const index = items.findIndex(i => i.id === id);
        const itemsCopy = [...items];
        const obj = itemsCopy[index];

        obj.count++;
        setItems(itemsCopy);
    };

    const countReduce = id => {
        const index = items.findIndex(i => i.id === id);
        const itemsCopy = [...items];
        const obj = itemsCopy[index];
        if (obj.count > 0) {
            obj.count--;
        } else {
            obj.count = 0
        }

        setItems(itemsCopy);
    };

    const totalPrice = () => {
        return items.reduce((acc, item) => {
            if (item.count > 0) {
                return acc + (item.price * item.count);
            }
            return acc;
        }, 20)
    };
    const showIngredients = name => {
        const arr = [];
        const index = items.findIndex(i => i.name === name)
        const itemsCopy = [...items];
        const ing = itemsCopy[index];
        const count = ing.count;

        for (let i = 0; i < count; i++) {
            arr.push(
                <Ingredients
                    key={arr.length}
                    name={ing.name}
                />
            );
        }

        return arr;
    }


    return (
        <Container maxWidth="md" className={classes.root}>
            <CssBaseline/>
            <Grid container direction="column" spacing={2}>
                {items.map(item => (
                    <Grid item key={item.id}>
                        <Paper component={Box} p={2}>
                            <Grid container justify="space-between" alignItems="center">
                                <Grid item>
                                    {item.name}: {item.count}
                                </Grid>

                                <Grid item>
                                    <button
                                        onClick={() => countIncrease(item.id)}>
                                        Add
                                    </button>
                                    <button
                                        onClick={() => countReduce(item.id)}>
                                        Reduce
                                    </button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                ))}
            </Grid>
            <Paper component={Box} p={3} mt={1}>
                <strong>Total price: {totalPrice()} KGS</strong>
            </Paper>
            <div className="Burger">
                <div className="BreadTop">
                    <div className="Seeds1"/>
                    <div className="Seeds2"/>
                </div>
                {showIngredients("Bacon")}
                {showIngredients("Cheese")}
                {showIngredients("Meat")}
                {showIngredients("Salad")}
                <div className="BreadBottom" />
            </div>


        </Container>
    );
};

export default App;